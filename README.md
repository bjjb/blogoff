Blog Off!
=========

While backpacking around South America, I met loads of people who were trying
to write travel blogs, but were invariably frustrated by the lack of reliable
internet access.  Doubltless the future holds free and omnipresent Wi-Fi
access for all, but in the meantime, it would be nice (I thought) to provide
these writers with a tool to let them compose their musings whenever the fancy
took them.

With HTML5, this is absolutely possible. A writer should be able to open their
blog _once_ (somewhere with an internet connection), and then continue to use
the page to "upload" photos and new articles even when the connection drops
(say, they need to board a bus). Then, when the Internet is available to them,
they can sync their articles, and carry on. No more waiting forever for their
blog's editor to load. Happier writers.

`blogoff` is my solution. It's a single page offline HTML5 application that
provides a standard blog composition interface, but keeps the data in the
user's local IndexedDB until they decide to sync it. At this point, their
changes get pushed up (as static HTML) to wherever they're hosting it.

[blogoff.herokuapp.com][] is provided as one such blog host.

A template is provided, modified from [HTML5Up][].

The source code is all available on [BitBucket][]. You can see it in action at
[blogoff.divshot.com][]. People are welcome to host it wherever they like, as
long as you abide by the [licence][].

[blogoff.herokuapp.com]: http://blogoff.herokuapp.com
[blogoff.divshot.io]: http://blogoff.divshot.io
[HTML5Up]: http://html5up.com
[BitBucket]: http://bitbucket.org/bjjb/blogoff
[licence]: https://bitbucket.org/bjjb/blogoff/raw/master/LICENSE.txt
