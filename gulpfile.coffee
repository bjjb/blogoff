gulp = require 'gulp'

gulp.task 'html', ->
  locals = {}
  jade = require 'gulp-jade'
  gulp.src('src/**/*.jade')
      .pipe(jade({locals}))
      .pipe(gulp.dest('www/'))

gulp.task 'css', ->
  stylus = require 'gulp-stylus'
  gulp.src('src/stylus/**/*.styl')
      .pipe(stylus())
      .pipe(gulp.dest('www/css/'))

gulp.task 'js', ->
  coffee = require 'gulp-coffee'
  gulp.src('src/coffee/**/*.coffee')
      .pipe(coffee())
      .pipe(gulp.dest('www/js/'))

gulp.task 'watch', ->
  watch = require 'gulp-watch'
  coffee = require 'gulp-coffee'
  gulp.src('src/coffee/**/*.coffee')
      .pipe(watch('src/coffee/**/*.coffee'))
      .pipe(coffee())
      .pipe(gulp.dest('www/js/'))

gulp.task 'default', ['html', 'css', 'js']
