# Application code for Blog Off!

throw NoPromise unless Promise?
throw NoIndexedDB unless indexedDB?
throw NoLocalStorage unless localStorage?

blogOff = (event) ->
  init().then(console.log).catch(console.error)

init = () ->
  config().then(db).then(load).then(sync).then(start)

config = ->
  Promise.resolve JSON.parse(localStorage.config or '{}')

db = (config) ->
  name =    config.db or 'Blog Off!'
  version = config.version or 1

  upgradeNeeded = (event) ->
    console.debug 'Database upgrade needed.', @
    if event.newVersion is 1
      console.debug 'Creating blogs...'
      blogs = @result.createObjectStore 'blogs', autoIncrement: true
      console.debug 'Creating posts...'
      posts = @result.createObjectStore 'posts', autoIncrement: true
      console.debug 'Creating images...'
      images = @result.createObjectStore 'images', autoIncrement: true
      console.debug 'Creating videos...'
      videos = @result.createObjectStore 'videos', autoIncrement: true
      console.debug 'Creating authorizations'
      authorizations = @result.createObjectStore 'authorizations', autoIncrement: true
      console.debug 'Creating index "blog" on posts...'
      posts.createIndex 'blog', 'blog'
      console.debug 'Creating index "post" on images...'
      images.createIndex 'post', 'post'
      console.debug 'Creating index "post" on videos...'
      videos.createIndex 'post', 'post'
    else
      console.error "Unknown database version #{event.newVersion}!"
      throw 'Invalid database version'
    console.debug 'Done!'

  new Promise (resolve, reject) ->
    success = (event) ->
      resolve new DB(@result)
    error = (event) ->
      reject @errorCode
    request = indexedDB.open(name, version)
    request.addEventListener 'upgradeneeded', upgradeNeeded, false
    request.addEventListener 'success', success, false
    request.addEventListener 'error', error, false

load = (db) ->
  db.all('blogs')

sync = (blogs) ->
  console.log blogs
  blogs

start = (blogs) ->
  console.log "Starting!"

DB = (db) ->
  all: (store) ->
    all = []
    new Promise (resolve, reject) ->
      request = db.transaction(store).objectStore(store).openCursor()
      request.addEventListener 'success', (event) ->
        return resolve(all) unless @result
        all.push(@result)
      request.addEventListener 'error', (event) ->
        return reject(@errorCode)

addEventListener 'DOMContentLoaded', blogOff, false
